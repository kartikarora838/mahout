package com.volcareTest.VolcareTest;

import java.io.File;
import java.util.List;

import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

public class VolcareApp 
{
	public static void main( String[] args ) throws Exception
    {
    	DataModel model = new FileDataModel(new File("data/volcareTestData.csv"));
    	UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
    	System.out.println("similarity"+similarity);
    	UserNeighborhood neighborhood = new NearestNUserNeighborhood(5, similarity, model);
    	System.out.println("neighborhood::"+neighborhood);
    	UserBasedRecommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);
    	System.out.println("recommender:"+recommender);
    	List<RecommendedItem> recommendations = recommender.recommend(2, 5);
    	System.out.println("recommendations"+recommendations);
    	for (RecommendedItem recommendation : recommendations) {
    		System.out.println(recommendation);
    	}
    }
}
